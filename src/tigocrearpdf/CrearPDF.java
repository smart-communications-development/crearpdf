package tigocrearpdf;
import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.ArrayList;
import com.lowagie.text.pdf.PdfWriter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
/**
 * @author Cesar Iglesias
 */
public class CrearPDF {
    public void Do(HashMap<String,Map> hmFacturas, List<String> listAnexos, boolean contieneB2B, String PDF_NAME, String CODCLIENTE, String nombreCarpeta) throws JRException {
//        System.out.println(PDF_NAME);
        String plantilla;
        JasperPrint jPrintPagina;
        JRPdfExporter jrpdfExporter = new JRPdfExporter();
        List<JasperPrint> jPrintUnidos = new ArrayList<>();
        Map emptyMap = new HashMap();
        Map parametersCarta1 = new HashMap(); //MapTemporal para ingresar parametros al jasper de Carta1
        Map parametersCarta2 = new HashMap(); //MapTemporal para ingresar parametros al jasper de Carta2
        Map parametersCarta3 = new HashMap(); //MapTemporal para ingresar parametros al jasper de Carta1    
        Map parametersCarta4 = new HashMap();
        Map datosPubli0 = new HashMap();
        Map datosPubli1 = new HashMap();
        String linkPublicidad = null;
        //boolean contieneB2B = false;
        try {
            //PUBLICIDAD QUE VA AL INICIO
//            if(contieneB2B==false) { //Meter la informal
//            jPrintPagina = JasperFillManager.fillReport(Inicio.DIRPROY+"\\jasper\\"+"cescb2c.jasper",emptyMap,new JREmptyDataSource());
//            jPrintPagina.setName("Información CESC");
//            jPrintUnidos.add(jPrintPagina);
//            }
//            else { //Meter la formal
//            jPrintPagina = JasperFillManager.fillReport(Inicio.DIRPROY+"\\jasper\\"+"cescb2b.jasper",emptyMap,new JREmptyDataSource());
//            jPrintPagina.setName("Información CESC");
//            jPrintUnidos.add(jPrintPagina);
//            }
            //FACTURA, CARTAS Y PUBLICIDAD PERSONALIZADA POR CUENTA
            for (String actualAnexo : listAnexos)
                try {
                     /////////////CARTA1
                
//                    if(Inicio.hmCarta1.containsKey(actualAnexo)) { //VARIABLES PARA CARTA1
//                        Map tempMapCA = Inicio.hmCarta1.get(actualAnexo); //Crea un MAP a partir del Hashmap Leido al inicio (Hasmap del tipo (String,Map)
//                        //System.out.println(cuotas_aplicadas_temp+"|"+tempMapCA.get("valCA5").toString());
//                        String valCA1 =  tempMapCA.get("valCA1").toString();
////                      parametersCarta1.put("valCA0", tempMapCA.get("valCA0").toString());
//                        parametersCarta1.put("valCA1", valCA1);
//                        parametersCarta1.put("valCA2", tempMapCA.get("valCA2").toString());
//                        parametersCarta1.put("valCA3", tempMapCA.get("valCA3").toString());
//                        parametersCarta1.put("valCA4", tempMapCA.get("valCA4").toString());
//                        parametersCarta1.put("valCA5", tempMapCA.get("valCA5").toString());
//                        parametersCarta1.put("valCA6", tempMapCA.get("valCA6").toString());
//                        parametersCarta1.put("valCA7", tempMapCA.get("valCA7").toString());
//                      
//                        if(valCA1.equals("CA007"))
//                            jPrintPagina = JasperFillManager.fillReport(Inicio.DIRPROY+"\\jasper\\"+"CA007.jasper",parametersCarta1,new JREmptyDataSource());
//                        else
//                            jPrintPagina = JasperFillManager.fillReport(Inicio.DIRPROY+"\\jasper\\"+"CA008.jasper",parametersCarta1,new JREmptyDataSource());
//                        jPrintPagina.setName("Aviso");
//                        jPrintUnidos.add(jPrintPagina);
//                        parametersCarta1 = new HashMap(); //Se borra completamente el MAP de Facturas una vez el anterior se insertó en el listado (Hashmap) de Facturas
//                    }
//
//                   
//                    
                    
                    /////////////CARTA2
//                    if(Inicio.hmCarta2.containsKey(actualAnexo)) { //VARIABLES PARA CARTA1
//                        Map tempMapCA = Inicio.hmCarta2.get(actualAnexo); //Crea un MAP a partir del Hashmap Leido al inicio (Hasmap del tipo (String,Map)
////                        System.out.println(tempMapCA.get("valCA4").toString());
////                       parametersCarta2.put("valCA0", tempMapCA.get("valCA0").toString());
//                        parametersCarta2.put("valCA1", tempMapCA.get("valCA1").toString());
//                        parametersCarta2.put("valCA2", tempMapCA.get("valCA2").toString());
//                        parametersCarta2.put("valCA3", tempMapCA.get("valCA3").toString());
//                        parametersCarta2.put("valCA4", tempMapCA.get("valCA4").toString());
////                        parametersCarta2.put("valCA5", tempMapCA.get("valCA5").toString());
////                        parametersCarta2.put("valCA6", tempMapCA.get("valCA6").toString());
////                        parametersCarta2.put("valCA7", tempMapCA.get("valCA7").toString());
//                        
//                        jPrintPagina = JasperFillManager.fillReport(Inicio.DIRPROY+"\\jasper\\"+"CA006.jasper",parametersCarta2,new JREmptyDataSource());
//                        jPrintPagina.setName("Aviso");
//                        jPrintUnidos.add(jPrintPagina);
//                        parametersCarta2 = new HashMap(); //Se borra completamente el MAP de Facturas una vez el anterior se insertó en el listado (Hashmap) de Facturas
//                    }


                    /////////////CARTA3
                   if(Inicio.hmCarta3.containsKey(actualAnexo)) { //VARIABLES PARA CARTA3
                       Map tempMapCA = Inicio.hmCarta3.get(actualAnexo); //Crea un MAP a partir del Hashmap Leido al inicio (Hasmap del tipo (String,Map)
                       //System.out.println(cuotas_aplicadas_temp+"|"+tempMapCA.get("valCA5").toString());
                       parametersCarta3.put("valCA0", tempMapCA.get("valCA0").toString());
                       parametersCarta3.put("valCA1", tempMapCA.get("valCA1").toString());
                       parametersCarta3.put("valCA2", tempMapCA.get("valCA2").toString());
                       parametersCarta3.put("valCA3", tempMapCA.get("valCA3").toString());
                       parametersCarta3.put("valCA4", tempMapCA.get("valCA4").toString());
                       parametersCarta3.put("valCA5", tempMapCA.get("valCA5").toString());
                       parametersCarta3.put("valCA6", tempMapCA.get("valCA6").toString());
                       parametersCarta3.put("valCA7", tempMapCA.get("valCA7").toString());
                       
                       //System.out.println(Inicio.DIRPROY+"\\jasper\\"+"CA010.jasper");
                       jPrintPagina = JasperFillManager.fillReport(Inicio.DIRPROY+"\\jasper\\"+"CA010.jasper",parametersCarta3,new JREmptyDataSource());
                       jPrintPagina.setName("Aviso");
                       jPrintUnidos.add(jPrintPagina);
                       parametersCarta3 = new HashMap(); //Se borra completamente el MAP de Facturas una vez el anterior se insertó en el listado (Hashmap) de Facturas
                   }
                   
                   
                    /////////////CARTA4 IN004
                    if(Inicio.hmCarta4.containsKey(actualAnexo)) { //VARIABLES PARA CARTA1
                        Map tempMapCA = Inicio.hmCarta4.get(actualAnexo); //Crea un MAP a partir del Hashmap Leido al inicio (Hasmap del tipo (String,Map)
////                        System.out.println(tempMapCA.get("valCA4").toString());
////                       parametersCarta2.put("valCA0", tempMapCA.get("valCA0").toString());
//                        parametersCarta2.put("valCA1", tempMapCA.get("valCA1").toString());
//                        parametersCarta2.put("valCA2", tempMapCA.get("valCA2").toString());
//                        parametersCarta2.put("valCA3", tempMapCA.get("valCA3").toString());
//                        parametersCarta2.put("valCA4", tempMapCA.get("valCA4").toString());
////                        parametersCarta2.put("valCA5", tempMapCA.get("valCA5").toString());
////                        parametersCarta2.put("valCA6", tempMapCA.get("valCA6").toString());
////                        parametersCarta2.put("valCA7", tempMapCA.get("valCA7").toString());
//                        
                        jPrintPagina = JasperFillManager.fillReport(Inicio.DIRPROY+"\\jasper\\"+"IN004.jasper",parametersCarta4,new JREmptyDataSource());
                        jPrintPagina.setName("Aviso");
                        jPrintUnidos.add(jPrintPagina);
                        parametersCarta4 = new HashMap(); //Se borra completamente el MAP de Facturas una vez el anterior se insertó en el listado (Hashmap) de Facturas
                    }
                   
                    
                    ///////////CREACION DE FACTURA
                    Map datosFacturaActual = hmFacturas.get(actualAnexo); //objeto que contiene los datos de cada factura en el hashmap                    
                    linkPublicidad = datosFacturaActual.get("linkPublicidad").toString();
//                    if(linkPublicidad.equals(Inicio.LINKB2B))
//                        contieneB2B = true;
                    switch (datosFacturaActual.get("TIPO_ARTE").toString()) {
                        case "arte0": plantilla="arte0.jasper";break;
                        case "arte1": plantilla="arte1.jasper";break;
                        default:      plantilla="arte1.jasper";break;
                    }
                    jPrintPagina = JasperFillManager.fillReport(Inicio.DIRPROY+"\\jasper\\"+plantilla,datosFacturaActual,new JREmptyDataSource());
                    jPrintPagina.setName(actualAnexo);
                    jPrintUnidos.add(jPrintPagina);
                } catch (JRException ex) {
                    utilidades.Log.set(0, "CrearPDF.java: for que recorre el hashmap: {0}"+Inicio.NL+
                            "ocurrió mientras se procesaba el anexo: " + actualAnexo, ex.getMessage());
                }
            /////////////////////PUBLICIDAD DE VA AL FINAL DEL PDF
            jPrintPagina = JasperFillManager.fillReport(Inicio.DIRPROY+"\\jasper\\"+"publicidad0.jasper",datosPubli0,new JREmptyDataSource());
            jPrintPagina.setName("Información");
            jPrintUnidos.add(jPrintPagina);
            if(contieneB2B==false) {
                if(listAnexos.size()<2)
                    datosPubli1.put("linkPublicidad", linkPublicidad);
                else
                    datosPubli1.put("linkPublicidad", Inicio.LINKB2B);
                jPrintPagina = JasperFillManager.fillReport(Inicio.DIRPROY+"\\jasper\\"+"publicidad1.jasper",datosPubli1,new JREmptyDataSource());
                jPrintPagina.setName("Información");
                jPrintUnidos.add(jPrintPagina);
            }
            jrpdfExporter.setExporterInput(SimpleExporterInput.getInstance(jPrintUnidos));
            jrpdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(Inicio.DIRPROY+"\\PDF\\"+nombreCarpeta+"\\"+PDF_NAME+".pdf"));
            SimplePdfExporterConfiguration expConfiguration = new SimplePdfExporterConfiguration();
            expConfiguration.setCreatingBatchModeBookmarks(true);
            expConfiguration.setPermissions(PdfWriter.ALLOW_COPY | PdfWriter.ALLOW_PRINTING | PdfWriter.ALLOW_MODIFY_CONTENTS);
            jrpdfExporter.setConfiguration(expConfiguration);
            jrpdfExporter.exportReport();
        } catch (JRException ex) {
            utilidades.Log.set(0, "Error en CrearPDF.java {0}", ex.getMessage());
        }
    }
}
package utilidades;
import java.io.File;
import java.awt.Desktop;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.Handler;
import java.text.SimpleDateFormat;
import java.util.logging.FileHandler;
import java.util.logging.SimpleFormatter;
public class Log {
   //Variables
        private static int err;
        private static FileHandler fh;
        private static String path,file;
        private final static Logger log = Logger.getLogger("GBM.Log");
    //Crea el log
        public static void set(int level,String firstmsg,String secondmsg){
            try{
                if(val(0)) initLog();
                if(val(1)){
                    log.log((level==0?Level.SEVERE:Level.INFO),firstmsg,secondmsg);
                    if(level==0) ++err;
                }
            }catch (Exception ex){
                System.out.println("(s) gbm.xml.Log - Log: "+ex.getMessage());
            }
        }
    //Obtiene la ruta del programa
        public static String progPath(){
            return System.getProperty("user.dir");
        }
        public static String xmlPath(){
            return "C:\\FTP\\GBM";
        }
    //Obtiene la ruta de los logs
        private static void path(){
            try{
                path = progPath()+"//Logs";
                new File(path).mkdirs();
            }catch (Exception ex){
                System.out.println("(p) gbm.xml.Log - Log: "+ex.getMessage());
            }
        }
        public static String getPath(){
            try{
                path();
                return path;
            }catch (Exception ex){
                System.out.println("(gp) gbm.xml.Log - Log: "+ex.getMessage());
                return "";
            }
        }
    //Nombre
        private static String filename(){
            return new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime())+"GBM.log";
        }
    //Inicializa el log
        private static void initLog(){
            try{
                c();
                path();
                fh = new FileHandler((file=path+"//"+filename()),true);
                fh.setEncoding("ISO-8859-1");
                log.setUseParentHandlers(true);
                log.addHandler(fh);
                fh.setFormatter(new SimpleFormatter());
            }catch (Exception ex){
                System.out.println("(il) gbm.xml.Log - Log: "+ex.getMessage());
            }
        }
    //Valida el log
        private static boolean val(int flag){
            if(flag==0) return (log.getHandlers().length<=0 || log==null);
            return (log!=null && log.getHandlers().length>0);
        }
    //Liquida el log
        private static void end(){
            for(Handler h:log.getHandlers()){
                h.close();
                log.removeHandler(h);
            }
            log.setUseParentHandlers(false);
        }
    //Finaliza el log
        public static void endLog(){
            try{
                if(val(1)) end();
                c();
            }catch (Exception ex){
                System.out.println("(el) gbm.xml.Log - Log: "+ex.getMessage());
            }
        }
    //Regresa la cantidad de errores registrados
        public static boolean ce(){
            return (err>0);
        }
    //Limpia la variable de errores
        public static void c(){
            err=0;
        }
        public static void cm(){
            --err;
        }
    //Abre el log
        public static void openLog(){
            try{
                File l=new File(file);
                if(Desktop.isDesktopSupported() && l.exists()) Desktop.getDesktop().open(l);
            }catch (Exception ex){
                System.out.println("(ol) gbm.xml.Log - Log: "+ex.getMessage());
            }
        }
    //Devuelve la ruta del log
        public static String logPath(){
            if(file==null || file.trim().equals("")) return "";
            return file;
        }
    //Devuelve la cantidad de errores ocurridas
        public static int cantErr(){
            return err;
        } 
}
package utilidades;
import java.util.Map;
import java.util.List;
import java.util.Date;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Iterator;
import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import tigocrearpdf.Inicio;
/**
 * @author Cesar Iglesias
 */
public class ExcelToHashmap {
    public static final String DIRPROY = System.getProperty("user.dir");
    public static void GetMarcaAgua() throws FileNotFoundException, IOException, InvalidFormatException  {
        Map paramMarcaAgua = new HashMap();
        try {
            boolean hayArchivos = false;
            File ruta = new File(DIRPROY);
            for (File archivo:ruta.listFiles()){
                if (xlsxValido(archivo)){
                    hayArchivos=true;
                    System.out.println("Leyendo Excel MARCA AGUA: " + archivo.getName());
                    //List<String> listAnexos  = new ArrayList<>(); //PARA PRUEBAS (ver "for" al final y linea donde se AÑADEN los anexos a la lista)
                    XSSFWorkbook workbook = new XSSFWorkbook(archivo);
                    XSSFSheet sheet = workbook.getSheetAt(0); //Pagina 0
                    Iterator<Row> rowIterator = sheet.iterator();
                    String s;
                    int colCount;
                    String anexo = "";
                    boolean seguir = true;
                    rowIterator.next(); //saltarse la primera línea (encabezado)
                   
                    while (rowIterator.hasNext()&&seguir) {
                        colCount = 0;
                        Row row = rowIterator.next();
                        Iterator<Cell> cellIterator = row.cellIterator();
                        while (cellIterator.hasNext()) {
                            Cell cell = cellIterator.next();
                            s=stringCelda(cell,true);
                            if (s.equals(""))
                                seguir=false;
                            else
                                if(colCount==1&&seguir) { //ANEXO en la segunda columna del libro, sino, cambiar numero
                                    anexo=s.trim();
//                                    System.out.println(anexo);
                                }
                                else {
                                    paramMarcaAgua.put("valMA"+colCount,s); //valorMarcaAgua
                                    
                                }
                            colCount++;
                        }
                        if (seguir) {
                           //listAnexos.add(anexo); //para pruebas (ver "for" al final)
//                            System.out.println(anexo);
                            Inicio.hmMarcaAgua.put(anexo, paramMarcaAgua);
                            paramMarcaAgua = new HashMap();
                        }
                    }
//                    for (String actualAnexo : listAnexos) { //PARA PRUEBAS (descomentar linea donde se declara listAnexos)
//                        System.out.println("Solicitando cod anexo: "+actualAnexo);
//                        Map tempMap = Inicio.hmMarcaAgua.get(actualAnexo);
//                        System.out.println("Monto Pendiente: "+tempMap.get("valMA6").toString());
//                    }
                } //FIN IF TXT VALIDO
            } //FIN FOR
            if(!hayArchivos)
                System.out.println(Inicio.NL+"ATENCION: No se encontró el archivo de base de marca de agua para procesar"+Inicio.NL+"NOTA: El Archivo .xlsx (no .xls) debe de contener la palabras \"BASE\"");
            
        } catch (IOException e) {
            utilidades.Log.set(0, "Main: {0}", e.getMessage());
        } finally{
//          file.close();
        }
    }
    public static boolean xlsxValido(File archivo) {
        return (!archivo.isDirectory()
            && !archivo.getName().toUpperCase().endsWith(".JAR") && !archivo.getName().toUpperCase().endsWith(".CMD") && !archivo.getName().toUpperCase().endsWith(".SH")
            && !archivo.getName().toUpperCase().startsWith("~$") && archivo.getName().toUpperCase().contains(".XLSX") && archivo.getName().toUpperCase().contains("BASE"));
    }
    public static String stringCelda(Cell c, boolean amp){
    String s="";
    if (c == null || c.getCellTypeEnum() == CellType.BLANK)
        return s;
    c.setCellType(CellType.STRING);
    switch (c.getCellTypeEnum()) {
        case STRING:
            s=c.getStringCellValue();
            break;
        case BOOLEAN:
            s=""+c.getBooleanCellValue();
            break;
        case NUMERIC:
            s=""+c.getNumericCellValue();
            break;
        case FORMULA:
            s=""+c.getCellFormula();
            break;
        default:
            s="-error-";
            break;
    }
    s=s.trim().replaceAll("\\s{2,}", " ");
    s=s.replaceAll("\n", " ");
    s=s.replace("	"," ");
    if(amp)
        s=s.replace("&", "&amp;");
    return s;
    }
//    public String fechaformato(String FECHA){
//        if (!FECHA.contains("/")){ //Convertir fecha de modo serial a dd/MM/yyyy
//            Date javaDate= DateUtil.getJavaDate((int)  Integer.parseInt(FECHA)); 
//            FECHA = new SimpleDateFormat("dd/MM/yyyy").format(javaDate);
//        }
//        return FECHA;
//    }
}
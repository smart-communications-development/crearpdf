package tests;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 *
 * @author Cesar Iglesias
 */
public class HashmapDeHashmaps {
    HashMap<String,Map> hmFacturas=new HashMap<>();
    Map parametersFactura = new HashMap();
    public void Do() {
        parametersFactura.put("CUENTA_FACTURACION","F01");
        hmFacturas.put("F01", parametersFactura);
        parametersFactura = new HashMap();
        parametersFactura.put("CUENTA_FACTURACION","F02");
        hmFacturas.put("F02", parametersFactura);
        parametersFactura = new HashMap();
        parametersFactura.put("CUENTA_FACTURACION","F03");
        hmFacturas.put("F03", parametersFactura);
        parametersFactura = new HashMap();
        //parametersFactura.clear();
        List<String> listAnexos = Arrays.asList("F01", "F02", "F03");
        for (String actualAnexo : listAnexos) {
            System.out.println("Solicitando cod anexo: "+actualAnexo+" cantidad de maps en el hashmap: "+ hmFacturas.size());
            Map parametersFactura2 = hmFacturas.get(actualAnexo);
            System.out.println("Codigo Cliente: "+parametersFactura2.get("CUENTA_FACTURACION").toString());
        }
    }
}
package tests;

import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.ArrayList;
import com.lowagie.text.pdf.PdfWriter;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
/**
 * @author Cesar Iglesias
 */
public class TIGO_FacturaManual {
    public static final String DIRPROY = System.getProperty("user.dir");
    public static void main(String[] args) throws JRException {
        Map parametersMap = new HashMap();
        JRPdfExporter exp = new JRPdfExporter();
        JasperPrint jasperPrint;
        String TAX_CORRELATIVE = "";
        String jasper = "test.jasper";
        List<JasperPrint> jasperPrintList = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            TAX_CORRELATIVE = "CCC_"+i;
            parametersMap.put("TAX_CORRELATIVE",TAX_CORRELATIVE);
            jasperPrint = JasperFillManager.fillReport(DIRPROY+"\\jasper\\"+jasper, parametersMap, new JREmptyDataSource());
            jasperPrintList.add(jasperPrint);
        }
        String ii = "A";
        //Mecanismo para importar varios Jasper en un solo PDF https://stackoverflow.com/questions/24115885/combining-two-jasper-reports
        exp.setExporterInput(SimpleExporterInput.getInstance(jasperPrintList)); //Set as export input my list with JasperPrint s
        exp.setExporterOutput(new SimpleOutputStreamExporterOutput(DIRPROY+"\\test\\"+ii+".pdf")); //or any other out streaam
        SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
        configuration.setCreatingBatchModeBookmarks(true); //add this so your bookmarks work, you may set other parameters
        configuration.setPermissions(PdfWriter.ALLOW_COPY | PdfWriter.ALLOW_PRINTING);
        exp.setConfiguration(configuration);
        exp.exportReport();
    }
}
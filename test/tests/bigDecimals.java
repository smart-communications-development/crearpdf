package tests;

import java.math.BigDecimal;

/**
 * @author Cesar Iglesias
 */
public class bigDecimals {
        public void Do() {
            BigDecimal acum = new BigDecimal("0.00");
            String total;

            acum = acum.add(sumStringMoney("10.01"));
            acum = acum.add(sumStringMoney("11.02"));
            acum = acum.add(sumStringMoney("12.03"));
            
            total = acum.toString();
            System.out.println(total);
        }
        public BigDecimal sumStringMoney(String s){ //Funcion que convierte strings a BigDecimal y valida si hubo un error
            BigDecimal total = new BigDecimal("0.00");
            try {
                total = total.add(new BigDecimal(s));
            }
            catch (NumberFormatException e) {
                total = new BigDecimal("0.00");
                System.out.print("Error de formato al intentar sumar: "+s+" | ");
                e.printStackTrace();
            }
            return total;
        }
}
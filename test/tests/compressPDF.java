package tests;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.pdfbox.cos.COSBase;
import org.apache.pdfbox.cos.COSDictionary;
import org.apache.pdfbox.cos.COSName;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
/**
 * @author https://stackoverflow.com/questions/53420344/how-to-reduce-the-size-of-merged-pdf-a-1b-files-with-pdfbox-or-other-java-librar
 */
public class compressPDF {
    public void Do() throws IOException, DocumentException {

        PdfReader reader = new PdfReader(new FileInputStream("201912_1282418_16733384_34662478.pdf"));
        PdfStamper stamper = new PdfStamper(reader, new FileOutputStream("201912_1282418_16733384_34662478_e.pdf"));
        int total = reader.getNumberOfPages() + 1;
        for ( int i=1; i<total; i++) {
           reader.setPageContent(i + 1, reader.getPageContent(i + 1));
        }
        stamper.setFullCompression();
        stamper.close();
        
//        File file = new File("201912_1282418_16733384_34662478.pdf");
//        PDDocument doc = PDDocument.load(file);
//        Map<String, COSBase> fontFileCache = new HashMap<>();
//        for (int pageNumber = 0; pageNumber < doc.getNumberOfPages(); pageNumber++) {
//            final PDPage page = doc.getPage(pageNumber);
//            COSDictionary pageDictionary = (COSDictionary) page.getResources().getCOSObject().getDictionaryObject(COSName.FONT);
//            for (COSName currentFont : pageDictionary.keySet()) {
//                COSDictionary fontDictionary = (COSDictionary) pageDictionary.getDictionaryObject(currentFont);
//                for (COSName actualFont : fontDictionary.keySet()) {
//                    COSBase actualFontDictionaryObject = fontDictionary.getDictionaryObject(actualFont);
//                    if (actualFontDictionaryObject instanceof COSDictionary) {
//                        COSDictionary fontFile = (COSDictionary) actualFontDictionaryObject;
//                        if (fontFile.getItem(COSName.FONT_NAME) instanceof COSName) {
//                            COSName fontName = (COSName) fontFile.getItem(COSName.FONT_NAME);
//                            fontFileCache.computeIfAbsent(fontName.getName(), key -> fontFile.getItem(COSName.FONT_FILE2));
//                            fontFile.setItem(COSName.FONT_FILE2, fontFileCache.get(fontName.getName()));
//                        }
//                    }
//                }
//            }
//        }
//        final ByteArrayOutputStream baos = new ByteArrayOutputStream();
//        doc.save(baos);
//        final File compressed = new File("201912_1282418_16733384_34662478_e.pdf");
//        baos.writeTo(new FileOutputStream(compressed));
    }
}
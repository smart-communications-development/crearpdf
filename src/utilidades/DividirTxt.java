package utilidades;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
public class DividirTxt {
    public boolean isNum(String val){
        try{
            Integer.parseInt(val);
            return true;
        }catch (Exception ex){
            ex.getMessage();
            return false;
        }
    }
    public boolean txtValido(File archivo){
        if(!archivo.isDirectory()
                && !archivo.getName().toUpperCase().endsWith(".JAR")
                && !archivo.getName().toUpperCase().endsWith(".LOG")
                && !archivo.getName().toUpperCase().startsWith("BASE")
                && archivo.getName().toUpperCase().contains(".TXT")
                && archivo.getName().toUpperCase().contains("PROCESADO")
                && archivo.getName().toUpperCase().contains("GBM"))
              return true;        
        else
            return false;
    }
    public void Do(String nombreArchivo, File archivo) {
        try{
            int i=0,div=1;
            File dir, folder;
            String dirProy = System.getProperty("user.dir");
            System.out.println("SE PROCEDE A SEPARAR EL ARCHIVO:\r\n"+nombreArchivo.substring(5)+".txt");
            System.out.println("Ingrese la cantidad de archivos en los que se dividira el TXT: ");
            String val=new BufferedReader(new InputStreamReader(System.in,"ISO-8859-1")).readLine();
            if(isNum(val))
                div=Integer.parseInt(val);            
            boolean correcto = true;
                if(archivo.toString().endsWith(".txt")){
                    int totalLinea=0;
                    try(BufferedReader br=new BufferedReader(new InputStreamReader(new FileInputStream(archivo),Charset.forName("ISO-8859-1")));){
                        while(br.readLine()!=null)
                           ++totalLinea;
                    }catch (Exception ex){
                        System.out.println(ex.getMessage()+"\r\n");
                        utilidades.Log.set(0, "DividirTxt.java,obteniendoNumLineas: {0}", ex.getMessage());
                        correcto = false;
                    }
                    int limite=(totalLinea/div),residuo=(totalLinea%div);
                    try(BufferedReader br=new BufferedReader(new InputStreamReader(new FileInputStream(archivo),Charset.forName("ISO-8859-1")));){
                        String str="";
                        StringBuilder sb=new StringBuilder();
                        int contador=0;
                        String cero = "0";
                        while((str=br.readLine())!=null){
                            sb.append((str+"\r\n"));
                            if((++contador)==(limite+(residuo>0?1:0))){
                                if(sb.length()>0){
                                    ++i;
                                    if (i<10)
                                        cero = "0";
                                    if (i>9)
                                        cero = "";
                                    dir = new File(dirProy+"\\"+nombreArchivo+"\\SEPARADOS"+"\\"+cero+i);
                                    folder = new File(dir.getAbsolutePath());
                                    folder.mkdir();
                                    try(BufferedWriter bw=new BufferedWriter(new OutputStreamWriter(new FileOutputStream(dirProy+"\\"+nombreArchivo+"\\SEPARADOS"+"\\"+cero+i+"\\BASE.txt"),"ISO-8859-1"))){
                                        bw.write(sb.toString());
                                        bw.flush();
                                        sb.delete(0,sb.length());                                        
                                        --residuo;
                                        contador=0;
                                    }catch (Exception ex){
                                        System.out.println(ex.getMessage()+"\r\n");
                                        utilidades.Log.set(0, "DividirTxt.java,creandoTXT: {0}", ex.getMessage());
                                        correcto = false;
                                    }
                                }
                            }
                        }
                    }catch (Exception ex){
                        System.out.println(ex.getMessage()+"\r\n");
                        utilidades.Log.set(0, "DividirTxt.java,BufferedReader: {0}", ex.getMessage());
                        correcto = false;
                    }
                }
            if(correcto){ //Finalmente Borrar todos los .txt, esto no se ejecutará si en algún momento el programa presentó un error.
                utilidades.Log.set(1, "DividirTxt.java: {0}", "Archivos Base creados");
//                for(File arch:ruta.listFiles())
//                    if(txtValido(arch))
//                          arch.delete();
            }
        }catch (Exception ex){
            System.out.println(ex.getMessage()+"\r\n");
            utilidades.Log.set(0, "DividirTxt.java: {0}", ex.getMessage());
        }
    }
}
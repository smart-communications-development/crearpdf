package tigocrearpdf;
import java.io.File;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.Calendar;
import java.util.ArrayList;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
public class GetVariables {
//    private static final DecimalFormat DFMB = new DecimalFormat("#.####"); //Para cuando toque formatear el valor en Megabytes
    private static final String LN = System.getProperty("line.separator");
    Map parametersHtml = new HashMap();
    Map parametersFactura = new HashMap();
    List<String> listAnexos = new ArrayList<>();
    HashMap<String,Map> hmFacturas=new HashMap<>();
    public boolean Do(File archivo, String nombreCarpeta) {
            int linea = 1;
            BigDecimal sumAfectas   = new BigDecimal("0.00");
            BigDecimal sumExentas   = new BigDecimal("0.00");
            BigDecimal sumNosujetas = new BigDecimal("0.00");
            BigDecimal SUM_SALDO_ANTERIOR          = new BigDecimal("0.00");    // (+)
            BigDecimal SUM_FACTURA_MES             = new BigDecimal("0.00");    // (-)
            BigDecimal SUM_PAGOS_ABONOS_REALIZADOS = new BigDecimal("0.00");    // (+)
            BigDecimal SUM_TOTAL_A_PAGAR           = new BigDecimal("0.00");    // (=)
            List<String> FECHA_DETALLE     = new ArrayList<>(); //Este es el orden en que se imprimen en el PDF las columnas de detalle de izquierda a derecha (layout/arte nuevo)
            List<String> DETALLE_CANTIDAD  = new ArrayList<>();
            List<String> DETALLE           = new ArrayList<>();
            List<String> VENTAS_NO_SUJETAS = new ArrayList<>();
            List<String> VENTAS_EXENTAS    = new ArrayList<>();
            List<String> VENTAS_AFECTAS    = new ArrayList<>();
            String ANEXO = "_", DETALLE_CANTIDAD_STR = "", REFERENCIA, TIPO_FACTURA, DETALLEDESCRIPCION,  str = "", CODCLIENTE = "_", PDF_NAME = "_", TIPO_ARTE = "";
            String NOMBRE_CLIENTE, FECHA_ESTADO_CUENTA, FECHA_VENCIMIENTO, DetalleHTML = "", HASH;
            String email, BUSSINES_UNIT="", SALDO_ANTERIOR, PAGOS_ABONOS_REALIZADOS, FACTURA_MES, TOTAL_A_PAGAR, NPE_IZQU="";
            String FechaBancosArteviejo = "", FechaTigoArteViejo = "";
            String cuotas_aplicadas_temp = "";
            boolean printDetalle, contieneB2B = false;
            int contadorFacturasHtml = 1;
            try(BufferedReader br=new BufferedReader(new InputStreamReader(new FileInputStream( archivo ),Charset.forName("ISO-8859-1")));){
                boolean primerRegistro = false; //para identificar si ya se leyeron los datos del primer registro
                while((str=br.readLine())!=null) {
                    switch (str.substring(0,4)) {
                        case "C000":
                            if (primerRegistro) { //si ya se recolectaron las variables del primer registro se creara el PDF y el HTML
                                //VARIABLES PARA EL HTML
                                parametersHtml.put("SUM_SALDO_ANTERIOR",SUM_SALDO_ANTERIOR.toString());
                                parametersHtml.put("SUM_PAGOS_ABONOS_REALIZADOS",SUM_PAGOS_ABONOS_REALIZADOS.toString());
                                parametersHtml.put("SUM_FACTURA_MES",SUM_FACTURA_MES.toString());
                                parametersHtml.put("SUM_CONSUMO_DEL_MES",SUM_TOTAL_A_PAGAR.toString());
                                parametersHtml.put("DetalleHTML", DetalleHTML);
                                parametersHtml.put("CANTIDAD_FACTURAS", listAnexos.size()+"");
                                SUM_SALDO_ANTERIOR = new BigDecimal("0.00");
                                SUM_PAGOS_ABONOS_REALIZADOS = new BigDecimal("0.00");
                                SUM_FACTURA_MES = new BigDecimal("0.00");
                                SUM_TOTAL_A_PAGAR = new BigDecimal("0.00");
                                //CREACION DE PDF Y LUEGO DE HTML
/* ------------------------ */                               new CrearPDF().Do(hmFacturas,listAnexos,contieneB2B,PDF_NAME,CODCLIENTE,nombreCarpeta);
                                new CrearHTML().Do(parametersHtml, PDF_NAME,nombreCarpeta);
                                contieneB2B = false;
                                DetalleHTML = "";
                                contadorFacturasHtml = 1;
                                listAnexos = new ArrayList<>();
                                hmFacturas.clear();
                                parametersHtml.clear();
                            }
                            primerRegistro = true; //se activa la bandera para aclarar que ya pasó el barrido por el primer registro
                            CODCLIENTE=str.substring(4,24).replaceFirst("^0+(?!$)", "");
                            parametersFactura.put("CODIGO_CLIENTE",CODCLIENTE); //remover los ceros a la izquierda de la string sin tener que convertir a int y de vuelta a string
                            PDF_NAME=str.substring(575,675).trim();
                            HASH=str.substring(54,154).trim();
                            parametersHtml.put("HASH",HASH);
                            break;
                        case "C010":
                            ANEXO = str.substring(24,44).replaceFirst("^0+(?!$)", "");                                    
                            REFERENCIA = str.substring(449,457).trim();
                            TIPO_FACTURA = str.substring(1254,1257).trim();
                            NOMBRE_CLIENTE = str.substring(44,104).trim();
                            FechaBancosArteviejo = str.substring(748, 758).trim();
                            FechaTigoArteViejo = str.substring(194, 204).trim();
                            //IF ARTE NUEVO
                            //FECHA_ESTADO_CUENTA = str.substring(194, 204).trim();
                            //ELSE (FECVEN para arte viejo)
                            FECHA_ESTADO_CUENTA = str.substring(2173,2183).trim();                            
                            FECHA_VENCIMIENTO = str.substring(748, 758).trim(); // FechaPagoBancos para Arte viejo
                            SALDO_ANTERIOR = str.substring(1764, 1779).trim().replace(",", ""); /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            PAGOS_ABONOS_REALIZADOS = str.substring(1794, 1809).trim().replace(",", ""); ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                            System.out.println(PAGOS_ABONOS_REALIZADOS);
                            FACTURA_MES = str.substring(1779, 1794).trim().replace(",", ""); //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            TOTAL_A_PAGAR = str.substring(1809, 1824).trim(); /////////////////////////////////////////////////////////////////////////////////////////////
                            parametersFactura.put("CODIGO_CLIENTE",str.substring(4,24).replaceFirst("^0+(?!$)", ""));
                            parametersFactura.put("CUENTA_FACTURACION",ANEXO);
                            parametersFactura.put("REFERENCIA",REFERENCIA);
                            parametersFactura.put("NOMBRE_CLIENTE",NOMBRE_CLIENTE);
                            parametersHtml.put("NOMBRE_CLIENTE",NOMBRE_CLIENTE);
                            parametersFactura.put("DIRECCION_CLIENTE",(str.substring(104,164)+str.substring(1334,1442)));
                            parametersFactura.put("GIRO",str.substring(457,496));
                            parametersFactura.put("TIPO_FACTURA",TIPO_FACTURA);
                            parametersFactura.put("TIPO_FACTURA_TEXTO",getTFL(TIPO_FACTURA)); //CCF COMPROBANTE DE CREDITO FISCAL, FCF FACTURA CONSUMIDOR FINAL, EXP FACTURA EXPORTACION
                            parametersFactura.put("TAX_CORRELATIVE",str.substring(424,444).trim()); //Estos llevan trim() porque no se vacian en un textbox que solamente lleva el parametro...
                            parametersFactura.put("Correlativo_impresion",str.substring(1734,1764).trim()); // ...sino que la variable en el textbox de jasper va acompañada de un "$" por lo que aquí no hace el trim automático
                            parametersFactura.put("GIRO",str.substring(457,496));
                            parametersFactura.put("SALDO_ANTERIOR", SALDO_ANTERIOR);
                            parametersFactura.put("PAGOS_ABONOS_REALIZADOS", PAGOS_ABONOS_REALIZADOS);
                            parametersFactura.put("FACTURA_MES", FACTURA_MES);
                            parametersFactura.put("CONSUMO_DEL_MES", str.substring(259, 269).trim().replace(",", "")); 
                            parametersFactura.put("MontoDeuda",TOTAL_A_PAGAR);
                            parametersFactura.put("TELEFONO",str.substring(164,194));
                            parametersFactura.put("PERIODO_SERVICIO",str.substring(1843,1849));
                            parametersFactura.put("FECHA_ESTADO_CUENTA","AL "+FECHA_ESTADO_CUENTA);
                            parametersFactura.put("FECHA_ESTADO_CUENTA-1",FECHA_ESTADO_CUENTA);
                            parametersHtml.put("FECHA_ESTADO_CUENTA",FECHA_ESTADO_CUENTA);
                            parametersFactura.put("FECHA_EMISION",str.substring(194, 204));
                            parametersFactura.put("FECHA_VENCIMIENTO",FECHA_VENCIMIENTO);
                            parametersHtml.put("FECHA_VENCIMIENTO",FECHA_VENCIMIENTO);
                            parametersFactura.put("REGISTRO_NO",str.substring(1849, 1879));
                            parametersFactura.put("CICLO",str.substring(251,259));
                            parametersFactura.put("LETRAS",str.substring(272,422));
                            parametersFactura.put("CESC_CONSUMO",str.substring(2159,2166));
                            parametersFactura.put("CESC_EQUIPO",str.substring(2166,2173));
                            if (TIPO_FACTURA.equals("CCF")){
                                parametersFactura.put("ValorRetencion",str.substring(1113,1123));
                                parametersFactura.put("IVA",str.substring(506,517));
                            }
                            else{
                                parametersFactura.put("ValorRetencion"," ");
                                parametersFactura.put("IVA"," ");
                            }
                            parametersFactura.put("NIT",str.substring(235,249));
                            parametersFactura.put("NET",getNET_NPE(str.substring(698,714),"-")); //Formatear el NET: 1234-1234-1234
                            NPE_IZQU = getNET_NPE(str.substring(758,788)," "); //Formatear NPE: 1234 1234 1234 (NPE Factura Actual)
                            parametersFactura.put("NPE_IZQU",NPE_IZQU);
                            parametersFactura.put("SUB_NPE_IZQU",str.substring(762,770));
                            parametersFactura.put("NPE_DERE",getNET_NPE(str.substring(1894,1924)," "));//Formatear NPE: 1234 1234 1234 (NPE Factura Actual + Facturas pendientes)
                            parametersFactura.put("SUB_NPE_DERE",str.substring(1898,1906));
                            parametersFactura.put("CUENTA_FACTURACION_CEROS",rS(ANEXO,8,"0",true)); //Rellenar String con ceros a la izquierda (para que siempre tenga longitud de 8)
                            parametersFactura.put("REFERENCIA_CEROS",rS(REFERENCIA,8,"0",true)); //Rellenar String con ceros a la izquierda (para que siempre tenga longitud de 8)
                            
                            //cambio pedido para ciclo 20210301
                            

                            parametersFactura.put("AUTORIZACION_IMPRESION",str.substring(1509,1634));
                            parametersFactura.put("AUTORIZACION_IMPRESION2",str.substring(1635,1733));
                            
//                            parametersFactura.put("AUTORIZACION_IMPRESION",str.substring(1509,1630));
//                            parametersFactura.put("AUTORIZACION_IMPRESION2",str.substring(1630,1733));
//                            System.out.println(str.substring(1509,1634));
//                            System.out.println(str.substring(1635,1733));
                            parametersFactura.put("INFO_IMPRENTA",str.substring(1934,2157));
                            if(str.subSequence(2184,2209).equals("MensajeDescuentoPromocion"))
                                parametersFactura.put("flagMA", "MensajeDescuentoPromocion");
                            
                            //System.out.println(ANEXO);
//                            if(Inicio.hmCarta3.containsKey(ANEXO))  //CINTILLO PARA HBO Y FOX
//                            {
//                                //System.out.println(ANEXO);
//                                parametersFactura.put("flagMA", "MensajeHBOFOX");}
                         
                            
//                            if(Inicio.hmCarta3.containsKey(ANEXO))  //CINTILLO PARA HBO Y FOX
//                            {
//                            Map tempMapMA = Inicio.hmCarta3.get(ANEXO);
////                                    System.out.println(cuotas_aplicadas_temp+"|"+tempMapMA.get("valMA5").toString());
//                                    if(tempMapMA.get("valCA1").toString()!=null){
//                                        parametersFactura.put("flagMA", "llevaHBOFOX");
//                                        parametersFactura.put("valCA1", tempMapMA.get("valCA1").toString());
//                                    //    System.out.println("para mensa " + tempMapMA.get("valCA1").toString());
//                                    
//                                     System.out.println(parametersFactura.get("valCA1"));
//                                    }  
//                                    else
//                                         parametersFactura.put("flagMA", "nollevaHBOFOX");
//                            
//                            }
//                            
                            //MENSAJE BURO 
//                            if(Inicio.hmCarta1.containsKey(ANEXO))  //Mensaje BURO
//                            {
//                            Map tempMapMA = Inicio.hmCarta1.get(ANEXO);
////                                    System.out.println(cuotas_aplicadas_temp+"|"+tempMapMA.get("valMA5").toString());
//                                    if(tempMapMA.get("valCA1").toString()!=null){
//                                        parametersFactura.put("flagMA", "llevaMENSAJEBURO");
//                                        //System.out.println("mensaje buro");
//                                    }  
//                                    else
//                                         parametersFactura.put("flagMA", "nollevaMENSAJEBURO");
//                            
//                            }
//                            
//                            //Amazon prime ME004
// 
                            if(Inicio.hmCarta2.containsKey(ANEXO))  //Mensaje BURO
                            {
                            Map tempMapMA = Inicio.hmCarta2.get(ANEXO);
//                                    System.out.println(cuotas_aplicadas_temp+"|"+tempMapMA.get("valMA5").toString());
                                    if(tempMapMA.get("valCA1").toString()!=null){
                                        parametersFactura.put("flagMA", "llevaMensajeAmazonePrime");
                                        //System.out.println("mensaje buro");
                                    }  
                                    else
                                         parametersFactura.put("flagMA", "nollevaMensajeAmazonePrime");
                            
                            }  
                            
//                            //llevaMENSAJEAumentoTarifa
//                            //Mensaje Aumento de tarifa ME006
                            if(Inicio.hmCarta1.containsKey(ANEXO))  //Mensaje BURO
                            {
                            Map tempMapMA = Inicio.hmCarta1.get(ANEXO);
//                                    System.out.println(cuotas_aplicadas_temp+"|"+tempMapMA.get("valMA5").toString());
                                    if(tempMapMA.get("valCA1").toString()!=null){
                                        //System.out.println(tempMapMA.get("valCA1").toString());
                                        parametersFactura.put("valCA1", tempMapMA.get("valCA1").toString());
                                        parametersFactura.put("flagMA", "llevaMensajeAumento");
                                        
                                       
                                        //System.out.println("mensaje buro");
                                    }  
                                    else
                                         parametersFactura.put("flagMA", "nollevaMensajeAumento");
                            
                            }

                            
                            try { //Suma de los subtotales usando BigDecimal para validar el formato moneda
                                if(!SALDO_ANTERIOR.trim().isEmpty())
                                    SUM_SALDO_ANTERIOR = SUM_SALDO_ANTERIOR.add(new BigDecimal(SALDO_ANTERIOR.replace(",", "")));
                                if(!PAGOS_ABONOS_REALIZADOS.trim().isEmpty())
                                    SUM_PAGOS_ABONOS_REALIZADOS = SUM_PAGOS_ABONOS_REALIZADOS.add(new BigDecimal(PAGOS_ABONOS_REALIZADOS.replace(",", "")));
                                if(!FACTURA_MES.trim().isEmpty())
                                    SUM_FACTURA_MES = SUM_FACTURA_MES.add(new BigDecimal(FACTURA_MES.replace(",", "")));
                                if(!TOTAL_A_PAGAR.trim().isEmpty())
                                    SUM_TOTAL_A_PAGAR = SUM_TOTAL_A_PAGAR.add(new BigDecimal(TOTAL_A_PAGAR.replace(",", "")));
                                //sumNosujetas = sumNosujetas.add(new BigDecimal(str.substring(15,26).trim()));  
                            } catch (Exception e) {
                                utilidades.Log.set(0, "ERROR al realizar suma en creacion de HTML en la linea: "+linea+" durante el Anexo: " + ANEXO+"| {0}", e.getMessage());
                            } 
                            break;
                        case "C011":
                            TIPO_ARTE = str.substring(4,7).trim();
                            if(TIPO_ARTE.equals("0")||TIPO_ARTE.equals("1")){
                                parametersFactura.put("FECHA_ESTADO_CUENTA",FechaBancosArteviejo);
                                parametersFactura.put("FECHA_EMISION",FechaTigoArteViejo);
                            }
                            BUSSINES_UNIT = str.substring(7,10).trim();
                            parametersFactura.put("BUSSINES_UNIT",BUSSINES_UNIT);
                            if (BUSSINES_UNIT.equals("0")||BUSSINES_UNIT.equals("1")||BUSSINES_UNIT.equals("2")||BUSSINES_UNIT.equals("6")||BUSSINES_UNIT.equals("7")||BUSSINES_UNIT.equals("8")){
                                contieneB2B = true;
                                parametersFactura.put("flagCESCB2C","no lleva");
                                parametersFactura.put("flagCESCB2B","lleva");
                            }
                            else {
                                parametersFactura.put("flagCESCB2C","lleva");
                                parametersFactura.put("flagCESCB2B","no lleva");
                            }
                            break;
                        case "C020": //Debido a las validaciones, para esta sección (lineas C020) las listas de los detalles no se ingresan en el orden de las columnas del PDF
                            printDetalle = printDetallefn(TIPO_ARTE, str.substring(26,96).trim());
                            if (printDetalle) {
                                DETALLEDESCRIPCION = str.substring(26,96);
                                DETALLE_CANTIDAD_STR = str.substring(143,154).trim();
                                if (!DETALLE_CANTIDAD_STR.equals("0"))
                                    DETALLE_CANTIDAD.add(DETALLE_CANTIDAD_STR);
                                else
                                    DETALLE_CANTIDAD.add(" "); //No dibujar valores "cero" en los detalles
                                DETALLE.add(DETALLEDESCRIPCION);
                                try { //Suma de los subtotales usando BigDecimal para validar el formato moneda
                                    if (!DETALLEDESCRIPCION.contains("Cuota por financiamiento de deuda")&&!DETALLEDESCRIPCION.contains("Eliminacion CESC en cuotas de financiamiento COVID pendientes")) {
                                            FECHA_DETALLE.add(str.substring(110,136).trim());
                                            VENTAS_NO_SUJETAS.add(" ");
                                            VENTAS_EXENTAS.add(str.substring(4,15));
                                            if(!str.substring(4,15).trim().isEmpty())
                                                sumExentas = sumExentas.add(new BigDecimal(str.substring(4,15).trim()));
                                            VENTAS_AFECTAS.add(str.substring(15,26));
                                            if(!str.substring(15,26).trim().isEmpty()) //Se suma el resultado a AFECTAS
                                                sumAfectas = sumAfectas.add(new BigDecimal(str.substring(15,26).trim()));
                                    } else {
                                        if (DETALLEDESCRIPCION.contains("Eliminacion CESC en cuotas de financiamiento COVID pendientes")) {
                                            FECHA_DETALLE.add(str.substring(110,136).trim());
                                            VENTAS_NO_SUJETAS.add(" ");
                                            VENTAS_EXENTAS.add(" ");
                                            VENTAS_AFECTAS.add(str.substring(4,15));
                                            if(!str.substring(4,15).trim().isEmpty()) //Se suma el resultado a AFECTAS
                                                sumAfectas = sumAfectas.add(new BigDecimal(str.substring(4,15).trim()));
                                        }
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                                        if (DETALLEDESCRIPCION.contains("Cuota por financiamiento de deuda")) {
                                        if  (DETALLEDESCRIPCION.contains("Cuota por financiamiento de deuda")&&(DETALLEDESCRIPCION.contains("("))) {
                                            String descTemp[] = DETALLEDESCRIPCION.split("\\(");
                                            String descTemp2[] = descTemp[1].split("de");
                                            cuotas_aplicadas_temp = descTemp2[0].trim();
                                            
                                            FECHA_DETALLE.add(" ");
                                            VENTAS_NO_SUJETAS.add(str.substring(15,26));
                                            VENTAS_EXENTAS.add(" ");
                                            VENTAS_AFECTAS.add(" ");
                                            if(!str.substring(15,26).trim().isEmpty()) //Se suma el resultado a NO SUJETAS
                                                sumNosujetas = sumNosujetas.add(new BigDecimal(str.substring(15,26).trim()));
                                        }
                                        
                                        
                                        
                                    }
//                                     else {
//                                        FECHA_DETALLE.add(str.substring(110,136).trim());
//                                        VENTAS_NO_SUJETAS.add(" ");
//                                        VENTAS_AFECTAS.add(str.substring(15,26));
//                                        if(!str.substring(15,26).trim().isEmpty()) //Se suma el resultado a AFECTAS
//                                            sumAfectas = sumAfectas.add(new BigDecimal(str.substring(15,26).trim()));                                            
//                                    }
                                } catch (Exception e) {
                                    utilidades.Log.set(0, "ERROR al realizar suma de subtotal durante el Anexo: " + ANEXO+"| {0}", e.getMessage());
                                }
                            }
                            break;
                        case "C030": //La linea que inicia con C030 indica que ya no hay mas detalles que añadir a las listas
                            /* Luego de haber añadido todos los detalles a la lista, se insertan al Map del Jasper */
                            parametersFactura.put("FECHA_DETALLE",FECHA_DETALLE);
                            parametersFactura.put("DETALLE_CANTIDAD",DETALLE_CANTIDAD);
                            parametersFactura.put("DETALLE",DETALLE);
                            parametersFactura.put("VENTAS_NO_SUJETAS",VENTAS_NO_SUJETAS);
                            parametersFactura.put("VENTAS_EXENTAS",VENTAS_EXENTAS);
                            parametersFactura.put("VENTAS_AFECTAS",VENTAS_AFECTAS);
                            /* Se limpian los detalles para que no se acumulen para el siguiente reporte generado */
                            FECHA_DETALLE     = new ArrayList<>();
                            DETALLE_CANTIDAD  = new ArrayList<>();
                            DETALLE           = new ArrayList<>();
                            VENTAS_NO_SUJETAS = new ArrayList<>();
                            VENTAS_EXENTAS    = new ArrayList<>();
                            VENTAS_AFECTAS    = new ArrayList<>();
                            /* Se insertan los totales de los detalles */
                            if(!sumNosujetas.toString().equals("0.00"))
                               parametersFactura.put("SUMA_NOSUJETAS",sumNosujetas.toString());
                            else
                                parametersFactura.put("SUMA_NOSUJETAS"," ");
                            if(!sumExentas.toString().equals("0.00"))
                                parametersFactura.put("SUMA_EXENTAS",sumExentas.toString());
                            else
                                parametersFactura.put("SUMA_EXENTAS", " ");
                            if(!sumAfectas.toString().equals("0.00"))
                                parametersFactura.put("SUMA_AFECTAS",sumAfectas.toString());
                            else
                                parametersFactura.put("SUMA_AFECTAS"," ");
                            sumNosujetas = new BigDecimal("0.00");
                            sumExentas   = new BigDecimal("0.00");
                            sumAfectas   = new BigDecimal("0.00");                            
                            email = str.substring(24,124).trim();
                            switch (TIPO_ARTE) {
                                case "2": parametersFactura.put("imgPublicidad","publi_1.jpg"); break;
                                case "3": parametersFactura.put("imgPublicidad","publi_2.jpg"); break;
                                default:            parametersFactura.put("imgPublicidad"," "); break;
                            }                            
                            switch (TIPO_ARTE) {
                                case "0": case "1": parametersFactura.put("TIPO_ARTE","arte0"); break;
                                case "2": case "3": parametersFactura.put("TIPO_ARTE","arte1"); break;
                                default:            parametersFactura.put("TIPO_ARTE","arte1"); break;
                            }
                            parametersFactura.put("linkPublicidad",getLinkFacturaPublicidad(BUSSINES_UNIT,ANEXO,email));
                            if(!TIPO_ARTE.equals("0")&&!TIPO_ARTE.equals("1"))
                                DetalleHTML = buildDetalleHtml(DetalleHTML,contadorFacturasHtml++,ANEXO,NPE_IZQU);
                            else 
                                DetalleHTML = " ";
                            if(Inicio.hmMarcaAgua.containsKey(ANEXO)) { //VARIABLES PARA MARCA DE AGUA (CUADRO PAGOS CUOTAS)
                                    Map tempMapMA = Inicio.hmMarcaAgua.get(ANEXO);
//                                    System.out.println(cuotas_aplicadas_temp+"|"+tempMapMA.get("valMA5").toString());
                                    if(cuotas_aplicadas_temp.equals(tempMapMA.get("valMA5").toString())){
                                        parametersFactura.put("flagMA", "lleva");
                                        parametersFactura.put("valMA2", tempMapMA.get("valMA2").toString());
                                        parametersFactura.put("valMA3", tempMapMA.get("valMA3").toString());
                                        parametersFactura.put("valMA4", tempMapMA.get("valMA4").toString());
                                        parametersFactura.put("valMA5", tempMapMA.get("valMA5").toString());
                                        parametersFactura.put("valMA6", tempMapMA.get("valMA6").toString());
                                    }
                                    else
                                         parametersFactura.put("flagMA", "nolleva");
                            }
                            hmFacturas.put(ANEXO, parametersFactura);
                            listAnexos.add(ANEXO);
                            parametersFactura = new HashMap(); //Se borra completamente el MAP de Facturas una vez el anterior se insertó en el listado (Hashmap) de Facturas
                            //parametersFactura.put("CODIGO_CLIENTE",str.substring(4,24).replaceFirst("^0+(?!$)", ""));
                            break;
                        default:
                            System.out.println("Linea en blanco o con formato erroneo: " + linea);
                            System.out.println("Ultimo codigo de factura (anexo) procesado: " + ANEXO);
                            break;
                    }
                    linea++;
                }
                linea = 0;
/* ------------------------ */               new CrearPDF().Do(hmFacturas,listAnexos,contieneB2B,PDF_NAME,CODCLIENTE,nombreCarpeta); //Crear PDF despues de leer el ULTIMO registro
                parametersHtml.put("SUM_SALDO_ANTERIOR",SUM_SALDO_ANTERIOR.toString());
                parametersHtml.put("SUM_PAGOS_ABONOS_REALIZADOS",SUM_PAGOS_ABONOS_REALIZADOS.toString());
                parametersHtml.put("SUM_FACTURA_MES",SUM_FACTURA_MES.toString());
                parametersHtml.put("SUM_CONSUMO_DEL_MES",SUM_TOTAL_A_PAGAR.toString());
                parametersHtml.put("DetalleHTML", DetalleHTML);
                parametersHtml.put("CANTIDAD_FACTURAS", listAnexos.size());
                new CrearHTML().Do(parametersHtml,PDF_NAME,nombreCarpeta); //Crear PDF despues de leer el ULTIMO registro
            }catch (Exception ex) {
                utilidades.Log.set(0, "ERROR: GetVariables.java,For de Lectura de txt: {0}"+LN+
                    "ocurrió en la línea: " + String.valueOf(linea) + LN + str, ex.getMessage());
                return false;
            }
            return true;

    }
    public String getLinkFacturaPublicidad(String bu, String ANEXO, String email) {
        String link;
        switch (bu) {
            case "3": case "4": case "5": link = Inicio.LINKB2C; break;
//            case "3": case "4": case "5": link = "https://pagos.tigo.com.sv/servicios/facturas/hogar?account="+ANEXO+"&email="+email; break;
            case "0": case "1": case "2": case "6": case "7": case "8": link = Inicio.LINKB2B; break;
            default: link = Inicio.LINKB2C; break;
        }
        return link;
    }
    
    public String getNET_NPE(String s, String separador){ //La variable del NET viene así: 123412341234... y hay que imprimirla así: 1234-1234-1234... (NPE es igual, pero sin guiones)
        int contador = 0;
        String NET = "";
        String[] s_array = s.split("");
        for (int i = 0; i < s.length(); i++) {
            if (contador == 4) {
                NET += separador + s_array[i];
                contador = 1;
            }
            else {
                NET += s_array[i];
                contador++;
            }
        }
        return NET;
    }
//    public String fechaMenosUno(String s) {
//        Calendar cal = Calendar.getInstance();
//        try {
//            cal.setTime(new SimpleDateFormat("dd/MM/yyyy").parse(s));
//            cal.add(Calendar.DAY_OF_MONTH, -1);
//            s = new SimpleDateFormat("dd/MM/yyyy").format(cal.getTime());
//        } catch (ParseException e) { utilidades.Log.set(0, "ERROR: funcion fechaMenosUno, al convertir el valor de fecha: "+ s +": {0}"+LN, e.getMessage()); }
//        return s;
//    }
    public String buildDetalleHtml(String detHtml, int num, String ANEXO, String NPE) {
        detHtml += "<div align=left><font size=3 face=\"Arial\" color=\"#000000\">";
        detHtml += Integer.toString(num) +" <b>ANEXO</b>&#58; "+ANEXO+" <b>NPE</b>&#58; "+NPE+"<br>";
        detHtml += "<br></font></div>";
        return detHtml;
    }
    public String getTFL(String tipo) { //Obtener TIPO_FACTURA_LETRAS
        switch(tipo){
            case "FCF":
                return "FACTURA CONSUMIDOR FINAL";
            case "EXP":
                return "FACTURA DE EXPORTACION";
            default:
                return "COMPROBANTE DE CREDITO FISCAL";
        }
    }
    public static String rS(String stringOri, Integer num, String relleno, boolean inicio){//String a rellenar, cantidad a rellenar, caracter con el que rellenar, relleno al inicio (true) o al final (false) //ejemplo: llenarEspacios("2342",3,"0",true), salida: 0002342
        String stringFin = stringOri;
        if(inicio)
            while(stringFin.length() < num){
                stringFin = relleno + stringFin;
           }
         else
            while(stringFin.length() < num){
                stringFin = stringFin + relleno;
            }
        return stringFin;
    }
//    public BigDecimal sumStringMoney(String s) { //Funcion que convierte strings a BigDecimal y valida si hubo un error
//        BigDecimal total = new BigDecimal("0.00");
//        try {
//            total = total.add(new BigDecimal(s));
//        }
//        catch (NumberFormatException e) {
//            utilidades.Log.set(0, "ERROR de formato al intentar sumar el valor: {0}"+s+"| ", e.getMessage());
//        }
//        return total;
//    }

    private boolean printDetallefn(String TIPO_ARTE, String detalle) {
        switch (TIPO_ARTE) {
            case "0": case "1":
                return true;
            default:
                return (!detalle.equals("CESC 5% COMPRA DE EQUIPO")&&!detalle.equals("CESC 5% CONSUMO"));
        }
    }
}
package tigocrearpdf;

import utilidades.ExcelToHashmap;
import utilidades.ExcelCarta1;
import utilidades.ExcelCarta2;
import utilidades.ExcelCarta3;
import utilidades.ExcelCarta4;
import utilidades.ExcelCarta5;
import java.io.File;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.HashMap;
public class Inicio {
    public static final String NL = System.getProperty("line.separator");
    public static final String DIRPROY = System.getProperty("user.dir");
    public static HashMap<String,Map> hmMarcaAgua=new HashMap<>();
    public static HashMap<String,Map> hmCarta1=new HashMap<>();
    public static HashMap<String,Map> hmCarta2=new HashMap<>();
    public static HashMap<String,Map> hmCarta3=new HashMap<>();
    public static HashMap<String,Map> hmCarta4=new HashMap<>();
    public static HashMap<String,Map> hmCarta5=new HashMap<>();
    public static final String LINKB2C = "https://www.tigo.com.sv/usa";
    public static final String LINKB2B = "https://www.tigobusiness.com.sv/";
    public static void main(String[] args) throws FileNotFoundException, IOException {
//        FileInputStream file = new FileInputStream(new File(DIRPROY+"\\BASE_20200916.xlsx"));
        try{
            utilidades.Log.c(); //Comentar para deshabilitar los Logs
            String nombreCarpeta;
            boolean correcto = false;
            boolean hayArchivos = false;
            File ruta = new File(DIRPROY);
/////////////////LECTURA DE EXCEL PARA OBTENER EL HASHMAP QUE CONTIENE LOS DATOS DE MARCA DE AGUA/////////////////

          ExcelToHashmap.GetMarcaAgua(); //0 en el excel
           System.out.println("Registros cuota financiamiento: "+hmMarcaAgua.size());
            
           ExcelCarta1.GetDatos();
            System.out.println("Registros carta 1: "+hmCarta1.size());
//            
//
            ExcelCarta2.GetDatos();
            System.out.println("Registros carta 2: "+hmCarta2.size());
////
            ExcelCarta3.GetDatos(); 
            System.out.println("Registros carta 3: "+hmCarta3.size());
//            System.out.println("//////////////////////////////////////////////////////////////////////////////////");
//           
            ExcelCarta4.GetDatos(); 
            System.out.println("Registros carta 4: "+hmCarta4.size());
//            System.out.println("//////////////////////////////////////////////////////////////////////////////////");  
//           
//            ExcelCarta5.GetDatos(); 
//            System.out.println("Registros carta 5: "+hmCarta5.size());
//            System.out.println("//////////////////////////////////////////////////////////////////////////////////");  
/////////////////LECTURA DE TXT PARA IR CREANDO LOS PDF/////////////////
            new File(DIRPROY+"//PDF").mkdirs();
            new File(DIRPROY+"//HTML").mkdirs();
            for(File archivo:ruta.listFiles()){ //
                if(txtValido(archivo)){
                    hayArchivos=true;
                    nombreCarpeta = archivo.getName().replace(".txt", "").replace(".TXT", "");
                    System.out.println("Procesando archivo txt: " + nombreCarpeta);
                    new File(DIRPROY+"//PDF//"+nombreCarpeta).mkdirs();
                    new File(DIRPROY+"//HTML//"+nombreCarpeta).mkdirs();
                    correcto = new GetVariables().Do(archivo, nombreCarpeta); //Aqui se capturan las variables para crear PDF
                }
            }
            if(!hayArchivos)
                System.out.println(NL+"ATENCION: No se encontraron archivos para procesar"+NL+"NOTA: El Archivo .txt debe de contener las palabras \"BASE\" y \"PDF\" y no debe de comenzar con la letra \"T\" (NO importa mayusculas o minusculas)");
            else
                if(correcto) //Finalmente Borrar todos los .txt procesados, esto no se ejecutará si en algún momento el programa presentó un error.
                    for(File archivo:ruta.listFiles())
                        if(txtValido(archivo)) {
                            utilidades.Log.set(1, "GetVariables.java: {0}", "Creación de PDF para el archivo \""+archivo.getName()+"\" [CORRECTA]");
                            //archivo.delete();  //Comentar esta linea si se desea EVITAR QUE BORRE el archivo original
            }
        } catch (Exception ex) {
            //System.out.println(ex.getMessage()+NL);
            utilidades.Log.set(0, "Main: {0}", ex.getMessage());
        } finally {
            utilidades.Log.endLog(); //Comentar para deshabilitar los Logs
        }
    }
    public static boolean txtValido(File archivo) {
        return (!archivo.isDirectory()
                && !archivo.getName().toUpperCase().endsWith(".JAR") && !archivo.getName().toUpperCase().endsWith(".CMD") && !archivo.getName().toUpperCase().endsWith(".SH") && !archivo.getName().toUpperCase().startsWith("T")
                && archivo.getName().toUpperCase().contains(".TXT")  && archivo.getName().toUpperCase().contains("BASE") && archivo.getName().toUpperCase().contains("PDF"));
    }
}
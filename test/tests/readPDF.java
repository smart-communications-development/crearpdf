package tests;

import java.io.File;
import java.io.IOException;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

/**
 * @author Cesar Iglesias
 */
public class readPDF {
    public void Do() throws IOException {
        PDDocument document = PDDocument.load(new File("CARTAS DE 90 SISA VIDA 27-02-2020.pdf"));
        if (!document.isEncrypted()) {
            PDFTextStripper stripper = new PDFTextStripper();
            //stripper.setSortByPosition(true);
            stripper.setPageStart("0");
            stripper.setEndPage(2);
//            stripper.setEndPage(document.getNumberOfPages());
            String text = stripper.getText(document);
            System.out.println(text);
        }
        document.close();
    }
}
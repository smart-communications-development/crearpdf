package tigocrearpdf;
import java.io.File;
import java.util.Map;
import java.util.Arrays;
import java.io.FileWriter;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.nio.charset.Charset;
import java.io.InputStreamReader;
/**
 * @author Cesar Iglesias
 */
public class CrearHTML {
     public void Do(Map parametersHtml, String PDF_NAME, String nombreCarpeta){
        File html_in = new File(Inicio.DIRPROY+"\\jasper\\template.html"); //Leer html y almacenar en string builder
        File html_out = new File(Inicio.DIRPROY+"\\HTML\\"+nombreCarpeta+"\\"+PDF_NAME+".html"); //Reemplazar el html anterior por el nuevo html con los cambios
        String lineahtml;
	FileWriter fwhtml = null;
        BufferedWriter bwhtml = null;
        StringBuilder sbhtml=new StringBuilder();
        try(BufferedReader brhtml=new BufferedReader(new InputStreamReader(new FileInputStream(html_in),Charset.forName("ISO-8859-1")));) {
            while((lineahtml=brhtml.readLine())!=null) {
                lineahtml=lineahtml.replace("[PDF_NAME]",PDF_NAME);
                lineahtml=lineahtml.replace("[NOMBRE_CLIENTE]",clean(parametersHtml.get("NOMBRE_CLIENTE").toString())); //Se le pasa la funcion clean() por si el nombre del cliente tiene caracteres especiales
                lineahtml=lineahtml.replace("[CANTIDAD_FACTURAS]",parametersHtml.get("CANTIDAD_FACTURAS").toString());
                lineahtml=lineahtml.replace("[FECHA_ESTADO_CUENTA]",parametersHtml.get("FECHA_ESTADO_CUENTA").toString());
                lineahtml=lineahtml.replace("[SUM_SALDO_ANTERIOR]",parametersHtml.get("SUM_SALDO_ANTERIOR").toString());
                lineahtml=lineahtml.replace("[SUM_PAGOS_ABONOS_REALIZADOS]",parametersHtml.get("SUM_PAGOS_ABONOS_REALIZADOS").toString());
                lineahtml=lineahtml.replace("[SUM_FACTURA_MES]",parametersHtml.get("SUM_FACTURA_MES").toString());
                lineahtml=lineahtml.replace("[SUM_CONSUMO_DEL_MES]",parametersHtml.get("SUM_CONSUMO_DEL_MES").toString());
                lineahtml=lineahtml.replace("[FECHA_VENCIMIENTO]",parametersHtml.get("FECHA_VENCIMIENTO").toString());
                lineahtml=lineahtml.replace("[DetalleHTML]",parametersHtml.get("DetalleHTML").toString());
                lineahtml=lineahtml.replace("[HASH]",parametersHtml.get("HASH").toString()+"0"); //Agrega cero al final, para que el server sepa que es descarga de email (1 es para sms)
                lineahtml = lineahtml+Inicio.NL; //Corregir caracteres especiales a formato html
                sbhtml.append(lineahtml);
            }
            try { //Escribir el contenido del string builder al archivo html (sobreescribir)
                fwhtml = new FileWriter(html_out);
                bwhtml = new BufferedWriter(fwhtml);
                bwhtml.write(sbhtml.toString().trim());
            } catch (IOException ex) {
                System.out.println("ERROR AL MOMENTO DE CREAR EL ARCHIVO HTML: "+PDF_NAME+".html");
                System.out.println(ex.getMessage()+Inicio.NL);
            }
        } catch (Exception ex) {
            System.out.println("ERROR EN LECTURA AL CREAR HTML: "+PDF_NAME+".html");
            System.out.println(ex.getMessage()+Inicio.NL);
        }
        finally {
            try {
                if (bwhtml != null)
                    bwhtml.close();
                if (fwhtml != null)
                    fwhtml.close();
            } catch (IOException ex) {
                System.out.println(ex.getMessage()+Inicio.NL);
            }
        }
     }
    private static String clean(String s){ //Limpia caracteres especiales del HTML
        //s=s.replace("'"," ").trim(); //esta función es de Jonathan, no se cual era la razón de reemplazar la comilla simple
        if(Arrays.asList(new String[]{"á","é","í","ó","ú","Á","É","Í","Ó","Ú","ä","ë","ï","ö","ü","Ä","Ë","Ï","Ö","Ü","Ñ","ñ","¡","•"}).parallelStream().anyMatch(s::contains)){
            return s.replace("á","&aacute;").replace("é","&eacute;").replace("í","&iacute;").replace("ó","&oacute;").replace("ú","&uacute;")
                    .replace("Á","&Aacute;").replace("É","&Eacute;").replace("Í","&Iacute;").replace("Ó","&Oacute;").replace("Ú","&Uacute;")
                    .replace("ä","&auml;").replace("ë","&euml;").replace("ï","&iuml;").replace("ö","&ouml;").replace("ü","&uuml;")
                    .replace("Ä","&Auml;").replace("Ë","&Euml;").replace("Ï","&Iuml;").replace("Ö","&Ouml;").replace("Ü","&Uuml;")
                    .replace("Ñ","&Ntilde;").replace("ñ","&ntilde;").replace("¡","&iexcl;").replace("•","-");
        }
    return s;
    }
}
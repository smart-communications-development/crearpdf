package tests;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * @author Cesar Iglesias
 */
public class stringTests {
    private static final String LN = System.getProperty("line.separator");
    public String fechaMenosUno(String s, String ANEXO){
        System.out.println(s);
        Calendar cal = Calendar.getInstance();
        try {
            cal.setTime(new SimpleDateFormat("dd/MM/yyyy").parse(s));
            cal.add(Calendar.DAY_OF_MONTH, -1);
            s = new SimpleDateFormat("dd/MM/yyyy").format(cal.getTime());
            System.out.println(s);
        } catch (ParseException e) { utilidades.Log.set(0, "ERROR: funcion fechaMenosUno, al convertir el valor de fecha: "+ s +": {0}"+LN+"en el ANEXO: " + String.valueOf(ANEXO), e.getMessage()); }
        return s;
    }
    public String BADfechaMenosUno(String s, String ANEXO){
        try { 
            String dia = s.substring(0, 2);
            String mes = s.substring(3, 5);
            String anno = s.substring(6, 10);
            System.out.println(dia+"/"+mes+"/"+anno);
            int day;
            int month;
            int year;

            day = Integer.parseInt(dia);
            month = Integer.parseInt(mes);
            year = Integer.parseInt(anno);
            if(day > 1)
                day -= 1;
            else {
                day = 1;
                if(month > 1)
                    month -= 1;
                else {
                    month = 1;
                    year -= 1;
                }
            }
            dia = Integer.toString(day);
            if(dia.length()<2)
                dia = "0"+dia;
            mes = Integer.toString(month);
            if(mes.length()<2)
                mes = "0"+mes;
            anno = Integer.toString(year);
            s = dia +"/"+ mes + "/" + anno;
            }
        catch (NumberFormatException e) {
            utilidades.Log.set(0, "funcion fechaMenosUno, al convertir el valor de fecha: "+ s +": {0}"+LN+"en el ANEXO: " + String.valueOf(ANEXO), e.getMessage());
            }
        System.out.println(s);
        return s;
    }
    public String getNET(String s){ //La variable del NET viene así: 123412341234... y hay que imprimirla así: 1234-1234-1234...
        int contador = 0;
        String NET = "";
        String[] s_array = s.split("");
        for (int i = 0; i < s.length(); i++) {
            if (contador == 4) {
                NET += "-" + s_array[i];
                contador = 1;
            }
            else {
                NET += s_array[i];
                contador++;
            }
        }
        return NET;
    }
    public String buildDetalleHtml(String detHtml, int num, String ANEXO, String NPE) {
        detHtml += Integer.toString(num) +" <b>ANEXO</b>&#58; "+ANEXO+" <b>NPE</b>&#58; "+NPE+"<br>";
        return detHtml;
    }
    public String fixDURATION_VOLUME(String val) {
        DecimalFormat fdseconds = new DecimalFormat("#0.00");
        DecimalFormat fdbytes = new DecimalFormat("#0.0000");
        if (val.toLowerCase().contains("second"))
            val = fdseconds.format((Double.parseDouble(val.replaceAll("[^\\d.]", ""))/60.00));
        if (val.toLowerCase().contains("bytes"))
            val = fdseconds.format((Double.parseDouble(val.replaceAll("[^\\d.]", ""))/1048576.00));
        if (val.toLowerCase().contains("items"))
            val = val.replaceAll("[^\\d.]", "");
        return val;
    }
    public String fixC030(String s){
        if (s.length() == 707){
            if (s.substring(696, 699).equals("-e-")) {
                 s = s.substring(0,624)+s.substring(665,707);
                
            }
            else{                
                s = s.substring(0,623)+s.substring(665,707)+" ";
            }
        }
        return s;
    }
    
}